import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  data: any = [
    { id: '#BK201', nombre: "Alcohol en gel", detalles: "500ml", stock: 23 },
    { id: '#AL404', nombre: "Paracetamol", detalles: "250mg", stock: 12 },
    { id: '#GN719', nombre: "Cubrebocas", detalles: "KN91", stock: 342 },
    { id: '#SH321', nombre: "Naproxeno", detalles: "500mg", stock: 43 }
  ]
}
