import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  users: any = [
    { name: "Yuki Kajiura", lastseen: 'online', id: "@daniellucario" },
    { name: "Ganyu Adeptus", lastseen: '08:12', id: "@galatea" },
    { name: "Schnee Schwarz", lastseen: '10:15', id: "@schneeschwarz" },
    { name: "Childe Nobile", lastseen: '16:14', id: "@tartaglia" }
  ]

  meessages: any = [
    { text: 'Mensaje de prueba', time: '14:15' },
    { text: 'Hola mundo', time: '14:18', mine: true },
    { text: 'Just testing', time: '16:58' }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
