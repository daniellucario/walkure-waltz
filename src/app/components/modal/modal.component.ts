import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ww-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor() { }

  hidden: boolean = true;

  ngOnInit(): void {
  }

  hide() {
    this.hidden = true;
  }

  show() {
    this.hidden = false;
  }

}
