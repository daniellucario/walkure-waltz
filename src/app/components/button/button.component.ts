import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ww-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() squared: boolean = false;
  @Input() pill: boolean = false;
  @Input() outline: boolean = false;
  @Input() color: string = 'default';

  constructor() { }

  ngOnInit(): void {
  }

}
