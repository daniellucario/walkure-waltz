import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { ButtonComponent } from './components/button/button.component';
import { TextComponent } from './components/text/text.component';
import { MenuComponent } from './pages/menu/menu.component';
import { ModalComponent } from './components/modal/modal.component';
import { LinkComponent } from './components/link/link.component';
import { ClickStopPropagationDirective } from './click-stop-propagation.directive';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { MatTabsModule } from '@angular/material/tabs';
import { InventoryComponent } from './tools/inventory/inventory.component';
import { ChatComponent } from './tools/chat/chat.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NotfoundComponent,
    ButtonComponent,
    TextComponent,
    MenuComponent,
    ModalComponent,
    LinkComponent,
    ClickStopPropagationDirective,
    InventoryComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    MatTabsModule
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
